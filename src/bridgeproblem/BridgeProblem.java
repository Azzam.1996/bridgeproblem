package bridgeproblem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 *
 * @author Batoul
 */
public class BridgeProblem {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
      /*  GraphList graph = new GraphList(5,5);
       graph.inputGraphFromKeyBoard();
       
//graph.generateRandomGraph();
        //graph.printGraph();
        /*
        Algorithm1 algo1 = new Algorithm1(graph);
        ArrayList<Edge> edges = algo1.solve();
        for(int i=0;i<edges.size();i++){
            System.err.println(edges.get(i).getU() + "   " + edges.get(i).getV());
        }
        
        
       long start = System.nanoTime();
        
       TarjanAlgorithmWithList tarjanAlgorithmWithList = new TarjanAlgorithmWithList(graph);
       tarjanAlgorithmWithList.solve();
       tarjanAlgorithmWithList.printBridges();
         long end = System.nanoTime();
        long elapsedTime = end - start;

        System.out.println("Times of algorithm 1 is " + elapsedTime);
        
        long start1 = System.nanoTime();
       Algorithm1WithList algo1 = new Algorithm1WithList(graph);
        ArrayList<Edge> edges = algo1.solve();
        for (int i = 0; i < edges.size(); i++) {
            System.out.println(edges.get(i).getU() + "   " + edges.get(i).getV());
        }

         long end1 = System.nanoTime();
        long elapsedTime1 = end1 - start1;
        System.out.println("Times of algorithm 2 is " + elapsedTime1);  */
        
        Graph graph1 = new Graph(1000,5000);
       /* graph1.addedge(0,1);
        graph1.addedge(1,2);
        graph1.addedge(2,0);
        graph1.addedge(1,3);
          graph1.addedge(1,4);
          graph1.addedge(1,6);
        graph1.addedge(3,5);
          graph1.addedge(4,5);*/
       // graph1.inputGraphFromKeyBoard();
       graph1.generateRandomGraph();
//graph1.printGraph();;
        
        long start3 = System.nanoTime();

        Algorithm1 algo2 = new Algorithm1(graph1);
        ArrayList<Edge> edges1 = algo2.solve();
        for (int i = 0; i < edges1.size(); i++) {
            System.out.println( "the bridage is 1:" +edges1.get(i).getU() + "   " + edges1.get(i).getV());
        }

        long end3 = System.nanoTime();
        long elapsedTime3 = end3 - start3;

        System.out.println("Times of algorithm 3 is " + elapsedTime3);
        
        
         long start4 = System.nanoTime();
        TarjansAlgorithm algo3= new TarjansAlgorithm(graph1);
        ArrayList<Edge> edges3 = algo3.solve();
        for (int i = 0; i < edges3.size(); i++) {
            System.out.println("the bridage is:"+edges3.get(i).getU() + "   " + edges3.get(i).getV());
        }
        
     long   end4  = System.nanoTime();
       long elapsedTime4 = end4 - start4;

        System.out.println("Times of algorithm 4 is " + elapsedTime4);

    }
    
    
}