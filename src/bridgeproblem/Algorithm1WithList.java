package bridgeproblem;

import java.util.ArrayList;

public class Algorithm1WithList {
    private GraphList graph;

    public Algorithm1WithList(GraphList graph) {
        this.graph = graph;
    }

    public boolean visited[];

    void dfs(int u) {
        visited[u] = true;
        for (int i = 0; i < graph.adjList.get(u).size(); i++) {
            int v = graph.adjList.get(u).get(i);
            if (visited[v] == false) {
                dfs(v);
            }
        }
    }

    public void clearVisited() {
        visited = new boolean[graph.getV()];
        for (int i = 0; i < graph.getV(); i++) {
            visited[i] = false;
        }
    }

    public boolean checkIfGraphConnected() {
        int count = 0;
        for (int i = 0; i < graph.getV(); i++) {
            if (visited[i]) {
                count++;
            }
        }
        if (count == graph.getV()) {
            return true;
        } else {
            return false;
        }
    }

    public ArrayList<Edge> solve() {
        ArrayList<Edge> edges = new ArrayList<>();

        for (int i = 0; i < graph.getV(); i++) {
            for (int j = i + 1; j < graph.getV(); j++) {
                if (graph.checkIfEdgeBetweenNodes(i, j)) {
                    graph.deleteEdge(i, j);
                    graph.deleteEdge(j, i);
                    clearVisited();
                    dfs(0);
                    
                    if(!checkIfGraphConnected()){
                        Edge e = new Edge(i, j);
                        edges.add(e);
                    }
                    
                    graph.adjList.get(i).add(j);
                    graph.adjList.get(j).add(i);
                    
                }

            }
        }

        return edges;
    }
    
}
