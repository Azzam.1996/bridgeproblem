
package bridgeproblem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.Scanner;

public class Graph {
    private int V; // number of vertex
    private int E; // number of edge
    
    public int[][] adj;

    public int getV() {
        return V;
    }

    public int getE() {
        return E;
    }

    public Graph(int V, int E) {
        this.V = V;
        this.E = E;
        adj = new int[V][V];
        
    }
      public void addedge(int u ,int  v){
  adj[u][v]=1;
adj[v][u]=1;
}
    public void removeedge(int u ,int v){
  adj[u][v]=0;
adj[v][u]=0;
   
        }  
    
    
    public void inputGraphFromKeyBoard() throws IOException{
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));      
        System.out.println("Please input edges as u v");
        for(int i=0;i<E;i++){
            String line = input.readLine();
            String[] tmp = line.split(" ");
            int u = Integer.parseInt(tmp[0]);
            int v = Integer.parseInt(tmp[1]);
            adj[u][v] = 1;
            adj[v][u] = 1;
   
        }     
    }
      public void generateRandomGraph() {
            Random rand = new Random();
            int u, v;
            int count = 0;
            while (count < this.E) {
                u = rand.nextInt(this.V);
                v = rand.nextInt(this.V);
                if (u != v && adj[u][v] == 0 && adj[v][u] == 0) {       
                    adj[u][v] = 1;
                    count++;
                }
            }           
        }
    
    
    public void printGraph(){
        for(int i=0;i<V;i++){
            for(int j=0;j<V;j++){
                if(adj[i][j] == 1){
                    System.out.println("(" + i + ", " + j + ")");
                }
            }
   
        } 
    }
    
    
    
    
    
    
}
