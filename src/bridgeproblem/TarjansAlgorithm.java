package bridgeproblem;

import java.util.ArrayList;

public class TarjansAlgorithm {

    private Graph graph;
    private boolean visited[];
    private int[] discoverNumber;
    private int lowNumber[];
    private int kTime;
     private ArrayList<Edge> bridges = new ArrayList<>();
     private int parent[];

    public TarjansAlgorithm(Graph graph) {
        this.graph = graph;
        visited = new boolean[graph.getV()];
        discoverNumber = new int[graph.getV()];
        lowNumber = new int[graph.getV()];
         parent = new int[graph.getV()];
        kTime = 0;
        for (int i = 0; i < graph.getV(); i++) {
            visited[i] = false;
            discoverNumber[i] = -1;
            lowNumber[i] = -1;
        }
    }

    void dfs(int u) {
        discoverNumber[u] = kTime;
        lowNumber[u] = kTime;
        kTime += 1;
        visited[u] = true;

        for (int i = 0; i < graph.getV(); i++) {
            if (graph.adj[u][i] == 1) {
                if (visited[i] == false) {
                    parent[u] = i;
                    dfs(i);
                    lowNumber[u] = Math.min(lowNumber[u], lowNumber[i]);
                } 
                 if (lowNumber[i] > discoverNumber[u]) {
                    bridges.add(new Edge(u, i));
                }
            } else if(i != parent[u]) {
                lowNumber[u] = Math.min(lowNumber[u], discoverNumber[i]);

            }
        }
    }
    
    
    public ArrayList<Edge> solve(){
        ArrayList<Edge> edges = new ArrayList<>();
        // compute discover and low number for each vertex
        for (int i = 0; i < graph.getV(); i++) {
            if(visited[i] == false){
                dfs(i);
            }
               return bridges;
        }
        
        for (int i = 0; i < graph.getV(); i++) {
           for(int j = i + 1;j<graph.getV();j++){
               if (graph.adj[i][j] == 1 && lowNumber[i] != lowNumber[j]){
                   edges.add(new Edge(i, j));
               }
           }
        }

        return bridges;
    }
    
    public void printBridges(){
        for(Edge e: bridges){
            System.out.println(e.toString());
        }
    }
}

