package bridgeproblem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

public class GraphList {

    private int V;
    private int E;
    public ArrayList<ArrayList<Integer>> adjList;

    public GraphList(int V, int E) {
        this.V = V;
        this.E = E;
        adjList = new ArrayList<>();
        for (int i = 0; i < this.V; i++) {
            adjList.add(new ArrayList<>());
        }
    }
//
      static void

    printGraph(ArrayList<ArrayList<Integer> > adj)

    {

        for (int i = 0; i < adj.size(); i++) {

            System.out.println("\nAdjacency list of vertex"

                               + i);

            System.out.print("head");

            for (int j = 0; j < adj.get(i).size(); j++) {

                System.out.print(" -> "

                                 + adj.get(i).get(j));

            }

            System.out.println();

        }
    }
   
    
    
    public void generateRandomGraph() {
        int[][] adjMatrix = new int[this.V][this.E];
        for (int i = 0; i < this.V; i++) {
            for (int j = 0; j < this.E; j++) {
                adjMatrix[i][j] = 0;
            }
        }

        int cnt = 0;
        Random random = new Random();
        for (int i = 0; i < this.V; i++) {
            for (int j = 0; j < this.E; j++) {
                if (i != j && adjMatrix[i][j] == 0) {
                   cnt++;
                    i = random.nextInt(this.V);
                j = random.nextInt(this.V);
                    adjMatrix[i][j] = 1;
                    adjMatrix[j][i] = 1;
                }
            }
        }

        for (int u = 0; u < this.V; u++) {
            for (int v = 0; v < this.E; v++) {
                if (u != v && adjMatrix[u][v] == 1) {
                    this.adjList.get(u).add(v);
                    this.adjList.get(v).add(u);

                }
            }
        }
 printGraph(this.adjList);
    }

    public void inputGraphFromKeyBoard() throws IOException {
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please input edges as u v");
        for (int i = 0; i <this.E; i++) {
            String line = input.readLine();
            String[] tmp = line.split(" ");
            int u = Integer.parseInt(tmp[0]);
            int v = Integer.parseInt(tmp[1]);
            this.adjList.get(u).add(v);
            this.adjList.get(v).add(u);

        }
          printGraph(this.adjList);

    }

    boolean checkIfEdgeBetweenNodes(int u, int v) {
        boolean flag = false;

        for (int i = 0; i < adjList.get(u).size(); i++) {
            if (adjList.get(u).get(i) == v) {
                return true;
            }
        }
        return flag;
    }

    public int getV() {
        return V;
    }

    public int getE() {
        return E;
    }

  public void deleteEdge(int u, int v) {
        if (checkIfEdgeBetweenNodes(u, v)) {
            for (int i = 0; i < adjList.get(u).size(); i++) {
                int node = adjList.get(u).get(i);
                if (node == v) {
                    adjList.get(u).remove(i);
                    break;
                }
            }
        }

    }
    
    

}
