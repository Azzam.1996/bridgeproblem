
package bridgeproblem;

public class Edge {
    private int u;
    private int v;

    public Edge(int u, int v) {
        this.u = u;
        this.v = v;
    }

    public int getU() {
        return u;
    }

    public int getV() {
        return v;
    }

    public void setU(int u) {
        this.u = u;
    }

    public void setV(int v) {
        this.v = v;
    }

    @Override
    public String toString() {
        return "Edge{" + "u=" + u + ", v=" + v + '}';
    }
    
    
    
    
    
}
