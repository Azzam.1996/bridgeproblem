package bridgeproblem;

import java.util.ArrayList;

public class Algorithm1 {

    private Graph graph;

    public Algorithm1(Graph graph) {
        this.graph = graph;
    }

    public boolean visited[];

    void dfs(int u) {
        visited[u] = true;

        for (int i = 0; i < graph.getV(); i++) {
            if (graph.adj[u][i] == 1 && visited[i] == false) {
                dfs(i);
            }
        }
    }

    public void clearVisited() {
        visited = new boolean[graph.getV()];
        for (int i = 0; i < graph.getV(); i++) {
            visited[i] = false;
        }
    }

    public boolean checkIfGraphConnected() {
        int count = 0;
        for (int i = 0; i < graph.getV(); i++) {
            if (visited[i]) {
                count++;
            }
        }
        if (count == graph.getV()) {
            return true;
        } else {
            return false;
        }
    }

    public ArrayList<Edge> solve() {
        ArrayList<Edge> edges = new ArrayList<>();

        for (int i = 0; i < graph.getV(); i++) {
            for (int j = i + 1; j < graph.getV(); j++) {
                if (graph.adj[i][j] == 1) {
                    graph.adj[i][j] = 0;
                    graph.adj[j][i] = 0;
                    clearVisited();
                    dfs(0);
                    
                    if(!checkIfGraphConnected()){
                        Edge e = new Edge(i, j);
                        edges.add(e);
                    }
                    graph.adj[i][j] = 1;
                    graph.adj[j][i] = 1;
                    
                }

            }
        }

        return edges;
    }

}
