package bridgeproblem;

import java.util.ArrayList;

public class TarjanAlgorithmWithList {

    private GraphList graph;
    private boolean visited[];
    private int parent[];
    private int[] discoverNumber;
    private int lowNumber[];
    private int kTime;
    private ArrayList<Edge> bridges = new ArrayList<>();

    public TarjanAlgorithmWithList(GraphList graph) {
        this.graph = graph;
        visited = new boolean[graph.getV()];
        discoverNumber = new int[graph.getV()];
        lowNumber = new int[graph.getV()];
        parent = new int[graph.getV()];
        kTime = 0;
        for (int i = 0; i < graph.getV(); i++) {
            visited[i] = false;
            discoverNumber[i] = -1;
            lowNumber[i] = -1;
        }
    }

    void dfs(int u) {
        discoverNumber[u] = kTime;
        lowNumber[u] = kTime;
        kTime += 1;
        visited[u] = true;

        for (int i = 0; i < graph.adjList.get(u).size(); i++) {
            int v = graph.adjList.get(u).get(i);
            if (visited[v] == false) {
                parent[v] = u;
               
                dfs(v);
                lowNumber[u] = Math.min(lowNumber[u], lowNumber[v]);
           
                
                if (lowNumber[v] > discoverNumber[u]) {
                    bridges.add(new Edge(u, v));
                }
            } else if(v != parent[u]) {
                lowNumber[u] = Math.min(lowNumber[u], discoverNumber[v]);
            }
        }
    }

    public ArrayList<Edge> solve() {
        for (int i = 0; i < graph.getV(); i++){
            parent[i] = -1;
        }
     
        // compute discover and low number for each vertex
        for (int i = 0; i < graph.getV(); i++) {
            if (visited[i] == false) {
                dfs(i);
            }
        }
        return bridges;
    }
    
    public void printBridges(){
        for(Edge e: bridges){
            System.out.println(e.toString());
        }
    }
}
